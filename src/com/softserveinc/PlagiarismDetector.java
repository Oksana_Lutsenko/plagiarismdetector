package com.softserveinc;

import java.util.List;

public interface PlagiarismDetector {
    int getPlagiarismPercent(List<String> input, List<String> output);
}
