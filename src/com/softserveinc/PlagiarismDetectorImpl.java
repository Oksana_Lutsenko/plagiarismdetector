package com.softserveinc;

import java.util.List;

public class PlagiarismDetectorImpl implements PlagiarismDetector {
    private int tupleLength;
    private List<List<String>> synonyms;

    public PlagiarismDetectorImpl(int tupleLength, List<List<String>> synonyms) {
        this.tupleLength = tupleLength;
        this.synonyms = synonyms;
    }

    @Override
    public int getPlagiarismPercent(List<String> file1, List<String> file2) {
        int tuplesCount = 0;
        int repeatedTuplesCount = 0;
        for (int i = 0; i <= file1.size() - tupleLength; i++) {
            if (isRepeatedTuple(file1.subList(i, i + tupleLength), file2)){
                repeatedTuplesCount++;
            }
            tuplesCount++;
        }
        return (int)((double)repeatedTuplesCount/tuplesCount * 100);
    }

    private boolean isRepeatedTuple(List<String> file1Tuple, List<String> file2) {
        for (int i = 0; i <= file2.size() - tupleLength; i++) {
            if (areTuplesEqual(file1Tuple, file2.subList(i, i + tupleLength))) {
                return true;
            }
        }
        return false;
    }

    private boolean areTuplesEqual(List<String> file1Tuple, List<String> file2Tuple) {
        for (int i = 0; i < file1Tuple.size(); i++) {
            if (areWordsNotEqual(file1Tuple.get(i), file2Tuple.get(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean areWordsNotEqual(String word1, String word2) {
        return !word1.equals(word2) && synonyms.stream()
                .filter(w -> w.contains(word1))
                .noneMatch(w -> w.contains(word2));
    }


}
