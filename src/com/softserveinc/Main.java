package com.softserveinc;

import com.softserveinc.util.HelpUtils;
import com.softserveinc.util.ParsingUtils;

import java.util.List;

public class Main {
    private static final int DEFAULT_TUPLE_SIZE = 3;

    public static void main(String[] args) {
        if (args.length == 0) {
            exitAppWithMessage(HelpUtils.getUsageInstructions());
        }
        List<List<String>> synonyms = parseFileByGroups(args[0], "File with synonyms does not exist. Please, check path and try again.");
        List<String> file1 = parseFile(args, 1, "Input file does not exist. Please, check path and try again.");
        List<String> file2 = parseFile(args, 2, "Output file does not exist. Please, check path and try again.");
        int tupleLength = DEFAULT_TUPLE_SIZE;
        if (args.length > 3) {
            tupleLength = getTupleSize(args[3]);
        }
        PlagiarismDetector plagiarismDetector = new PlagiarismDetectorImpl(tupleLength, synonyms);
        int plagiarismPercent = plagiarismDetector.getPlagiarismPercent(file1, file2);
        System.out.println(plagiarismPercent + "% plagiarism");
    }

    private static List<String> parseFile(String[] args, int i, String s) {
        try {
            return ParsingUtils.parseInputFile(args[i]);
        } catch (IllegalArgumentException e) {
            exitAppWithMessage(s);
            throw e;
        }
    }

    private static List<List<String>> parseFileByGroups(String filePath, String errorMessage) {
        try {
            return ParsingUtils.parseInputFileByGroups(filePath);
        } catch (IllegalArgumentException e) {
            exitAppWithMessage(errorMessage);
            throw e;
        }
    }

    private static int getTupleSize(String args) {
        try {
            return Integer.parseInt(args);
        } catch (NumberFormatException e) {
            exitAppWithMessage("Tuple size is not integer. Please, check and try again");
            throw e;
        }
    }

    private static void exitAppWithMessage(String message) {
        System.out.println(message);
        System.exit(0);
    }
}
