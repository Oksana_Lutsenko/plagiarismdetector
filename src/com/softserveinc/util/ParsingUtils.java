package com.softserveinc.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ParsingUtils {
    private static final String WORD_SEPARATOR = " ";

    public static List<String> parseInputFile(String path) {
        try {
            return Files.lines(Paths.get(path))
                    .flatMap(str -> Arrays.stream(str.split(WORD_SEPARATOR)))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    public static List<List<String>> parseInputFileByGroups(String path) {
        try {
            return Files.lines(Paths.get(path))
                    .map(str -> Arrays.asList(str.split(WORD_SEPARATOR)))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
