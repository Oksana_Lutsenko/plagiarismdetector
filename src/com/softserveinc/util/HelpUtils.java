package com.softserveinc.util;

public class HelpUtils {
    public static String getUsageInstructions() {
        return "Please, specify 3 params: 1.file name for a list of synonyms" +
                System.lineSeparator() +
                "2.input file 1" +
                System.lineSeparator() +
                "3.input file 2";
    }
}
